# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Install Needed Package

```
sudo yum install python-setuptools
sudo easy_install pip
sudo pip install supervisor
```

Setup Supervisor

```
echo_supervisord_conf > supervisord.conf
sudo cp supervisord.conf /etc/supervisord.conf
sudo mkdir /etc/supervisord.d/
sudo vi /etc/supervisord.conf
:
[program:laravel-worker]
directory=/path/to/sourcecode
process_name=%(program_name)s_%(process_num)02d
command=php artisan queue:work --sleep=1 --tries=3
autostart=true
autorestart=true
user=root
numprocs=8
redirect_stderr=true
stdout_logfile=/path/to/sourcecode/storage/logs/worker.log
:
```

```
sudo vi /etc/rc.d/init.d/supervisord
#!/bin/sh
#
# /etc/rc.d/init.d/supervisord
#
# Supervisor is a client/server system that
# allows its users to monitor and control a
# number of processes on UNIX-like operating
# systems.
#
# chkconfig: - 64 36
# description: Supervisor Server
# processname: supervisord

# Source init functions
. /etc/rc.d/init.d/functions

prog="supervisord"

prefix="/usr/"
exec_prefix="${prefix}"
prog_bin="${exec_prefix}/bin/supervisord"
PIDFILE="/var/run/$prog.pid"

start()
{
       echo -n $"Starting $prog: "
       daemon $prog_bin --pidfile $PIDFILE
       [ -f $PIDFILE ] && success $"$prog startup" || failure $"$prog startup"
       echo
}

stop()
{
       echo -n $"Shutting down $prog: "
       [ -f $PIDFILE ] && killproc $prog || success $"$prog shutdown"
       echo
}

case "$1" in

 start)
   start
 ;;

 stop)
   stop
 ;;

 status)
       status $prog
 ;;

 restart)
   stop
   start
 ;;

 *)
   echo "Usage: $0 {start|stop|restart|status}"
 ;;

esac
```
```
sudo chmod +x /etc/rc.d/init.d/supervisord
sudo chkconfig --add supervisord
sudo chkconfig supervisord on
sudo service supervisord start
```

Checking Supervisor
```
sudo supervisorctl status all
laravel_worker:laravel_worker_00   RUNNING   pid 6747, uptime 0:01:34

sudo supervisorctl stop laravel_worker:*
laravel_worker:laravel_worker_00: stopped

sudo supervisorctl start laravel_worker:*
laravel_worker:laravel_worker_00: started

```

Example:

[program:laravel_worker]
directory=/var/www/html//sourcecode
process_name=%(program_name)s_%(process_num)02d
command=php artisan queue:work --tries=1 --timeout=600 --sleep=5
autostart=true
autorestart=true
user=root
numprocs=8
redirect_stderr=true
stdout_logfile=/var/www/html//sourcecode/storage/logs/worker.log